﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_Login.Common_Repository.Interface
{
    public interface Igetuser<TEntity> where TEntity : class
    {
        Task<TEntity> getuser(TEntity entityObj);
    }
}
