﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_Login.Common_Repository.Interface
{
    public interface Iselect<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> Select(TEntity entityObj);
    }
}
