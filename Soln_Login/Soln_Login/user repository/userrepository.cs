﻿using Soln_Login.Concrete;
using Soln_Login.Concrete.Interface;
using Soln_Login.Entity;
using Soln_Login.Entity.Interface;
using Soln_Login.user_repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_Login.user_repository
{
     public class userrepository : Iuserrepository
    {
         #region Declaration
        private Iconcrete userconcreteobj = null;
        #endregion

        #region Constructor
        public userrepository()
        {
            userconcreteobj = new concrete();
        }
        #endregion

        public async Task<IEnumerable<Iloginentity>> Select(Iloginentity entityObj)
         {
             int? status = null;
             String message = null;
             try
             {
                 var getQuery =
                         (await
                             userconcreteobj
                             
                                 .getuserdata(
                                     "Select"
                                     , entityObj
                                     , (leuspgetuserobj) => new loginentity()
                                     {
                                         username = leuspgetuserobj.username,
                                         password = leuspgetuserobj.password

                                     })) as IEnumerable<Iloginentity>;

                 return getQuery;
             }
             catch (Exception)
             {
                 throw;
             }
         }
    }
}
