﻿using Soln_Login.Concrete.Interface;
using Soln_Login.Entity.Interface;
using Soln_Login.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_Login.Concrete
{
   public class concrete : Iconcrete
    {
       #region Delcaration
        private userloginDataContext Dc = null;
        #endregion

        #region Constructor
        public concrete()
        {
            Dc = new userloginDataContext();
        }

        public async Task<dynamic> getuserdata(string command, Iloginentity entityObj, Func<uspcustom, Iloginentity> selector, Action<int?, string> storedProcedureOutPara)
        {
            int? status = null;
            String message = null;
            try
            {
                return await Task.Run(()=> {

                    var getQuery =
                        Dc
                        .uspgetuser
                        (
                                    command
                                   , entityObj.userid
                                   
                                   , entityObj.username
                                   , entityObj.password
                                    
                                   , ref status
                                   , ref message
                            )
                            .AsEnumerable()
                            .FirstOrDefault()
                           
                            .Select((leEntity)=> selector(leEntity))
                            .ToList();
                            

                    return getQuery;
                });
            }
            catch(Exception)
            {
                throw;
            }

        }
        #endregion

   
    }
}
