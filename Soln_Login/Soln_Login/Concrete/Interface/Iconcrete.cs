﻿using Soln_Login.Common_Repository.Interface;
using Soln_Login.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_Login.Concrete.Interface
{
    public interface Iconcrete : Iselect<Iloginentity>, Igetuser<Iloginentity>
    {

    }
}