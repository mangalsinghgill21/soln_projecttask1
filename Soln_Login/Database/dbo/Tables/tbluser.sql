﻿CREATE TABLE [dbo].[tbluser] (
    [userid]   NUMERIC (18)  NOT NULL,
    [username] VARCHAR (50)  NOT NULL,
    [password] VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tbluser] PRIMARY KEY CLUSTERED ([userid] ASC)
);

